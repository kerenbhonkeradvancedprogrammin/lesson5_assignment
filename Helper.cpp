#include "Helper.h"


void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

std::vector<std::string> Helper::get_words(std::string &str)
{
    std::vector<std::string> words;
	std::istringstream strStream(str);
    std::copy(std::istream_iterator<std::string>(strStream),
        std::istream_iterator<std::string>(),
        std::back_inserter(words));

	return words;
}

