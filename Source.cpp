#include "Helper.h"
#include <Windows.h>
#include <iostream>
#include <string>
#include <fstream>

typedef std::vector<std::string> stringvec;
typedef int(__cdecl *MYPROC)(LPWSTR);
using namespace std;

#define MAX_LEN 2048

vector<string> getInput();
void pwd();
void cd(string path);
void create(string name);
void read_directory(const std::string& name, stringvec& v);
void ls();
void secret();
void exe(string file);

int main()
{
	static const basic_string <char>::size_type npos = -1;

	while (true)
	{
		vector<string> input = getInput();	//get input from user
		string command = "", msg = "";
		size_t point;
		
		if (!input.empty())	//if the user wrote something
		{
			command = input.at(0);
			point = input.at(0).find_last_of(".");	//find the last point in the input
			msg = (point != npos) ? input.at(0).substr(point) : "";	//if there is a point in the input, cut the string. 
																	//for example: "example.exe" => ".exe"
		}

		if (command.compare("pwd") == 0)
		{
			pwd();
		}
		else if (command.compare("cd") == 0 && input.size() == 2)
		{
			//check if the user wrote 2 words - the command and the input
			//for example: "cd C:\Users"
			cd(input.at(1));
		}
		else if (command.compare("create") == 0 && input.size() == 2)
		{
			//check if the user wrote 2 words - the command and the input
			//for example: "create keren.txt"
			create(input.at(1));
		}
		else if (command.compare("ls") == 0)
		{
			ls();
		}
		else if (command.compare("secret") == 0)
		{
			secret();
		}
		else if (msg.compare(".exe") == 0) 
		{
			//check if the file's type is exe
			exe(command);
		}
	}
}

/*
Function gets input from the user
input:	None.
output:	a vector of words
*/
vector<string> getInput()
{
	string input = "";

	std::cout << ">> ";	
	std::getline(std::cin, input);	//get the input
	Helper::trim(input);	//removes whitespace from the begining and the end of the string
	
	//return a vector of words 
	//("for example" vec[0]:"for" vec[1]:"example"
	return Helper::get_words(input);
}

/*
Function prints the current directory
input: None.
output:	 None.
*/
void pwd()
{
	char path[MAX_LEN];
	GetCurrentDirectory(MAX_LEN, path);	//get the current directory
	std::cout << path << std::endl;	//print the directory
}

/*
Function changes the current directory to the directory that the user wrote
input:	the new directory
output:	None.
*/
void cd(string path)
{
	SetCurrentDirectory(path.c_str());
}

/*
Function creates a new file. if the file already exists - the function deletes it and create a new one.
input:	the file's name
output:	None.
*/
void create(string name)
{
	std::ofstream o(name);
}

/*
Function reads the directory and adds all the files' names to a vector
input: the name of the directory, vector
output: None
*/
void read_directory(const std::string& name, stringvec& v)
{
	std::string pattern(name);
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			v.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}

/*
Function prints a list of all the files in the directory
input:	None.
output:	None.
*/
void ls()
{
	stringvec v;
	read_directory(".", v);	//read the directory
	std::copy(v.begin(), v.end(),std::ostream_iterator<std::string>(std::cout, "\n"));
}

/*
Function calls to the secret function "TheAnswerToLifeTheUniverseAndEverything" and prints its return value
input:	None.
output: None.
*/
void secret()
{
	HINSTANCE hinstLib;
	MYPROC ProcAdd;

	// Get a handle to the DLL module.
	hinstLib = LoadLibrary(TEXT("Secret.dll"));
	// If the handle is valid, try to get the function address.
	if (hinstLib != NULL)
	{
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything");
		// If the function address is valid, call the function.
		if (NULL != ProcAdd)
		{
			std::cout << (ProcAdd)(L"") << std::endl;	//print the return value
		}
		// Free the DLL module.
		FreeLibrary(hinstLib);
	}
}

/*
Function runs an exe file and prints its exit code
input:	the file's name
output:	None.
*/
void exe(string file)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD code;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the process. 
	if (!CreateProcess(file.c_str(),   //the file's name
		NULL,        
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return;
	}

	// Wait until process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);	//If the function succeeds, the return value indicates the event that caused the function to return
	
	//The function getExitCodeProcess retrieves the termination status of the specified process.
	//If the function succeeds, the return value is nonzero. If the function fails, the return value is zero
	std::cout << "Exit code: " << GetExitCodeProcess(pi.hProcess, &code) << std::endl;	//print the exit code
	
	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}